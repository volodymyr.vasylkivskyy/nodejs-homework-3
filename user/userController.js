const bcrypt = require('bcrypt');
const {User} = require('../user/userModel');

/**
 * Get User Info
 * @param {object} req
 * @param {object} res
 */
async function getUser(req, res) {
  if (req.user) {
    res.status(200).json({user: req.user});
  } else {
    res.status(400).json({message: 'Authentification needed!'});
  }
}

/**
 * Delete User
 * @param {obj} req
 * @param {obj} res
 */
async function deleteUser(req, res) {
  if (req.user.role === 'SHIPPER') {
    await User.findByIdAndDelete(req.user._id);
    res.json({message: 'Profile deleted successfully'});
  } else {
    res.json({message: 'Sorry,you cant delete your account'});
  }
}

/**
 * Change Pass
 * @param {obj} req
 * @param {obj} res
 */
async function changePassword(req, res) {
  const user = await User.findOne({email: req.user.email});
  console.log(user);
  if (!(await bcrypt.compare(req.body.oldPassword, user.password))) {
    return res.status(400).json({message: `Wrong password!`});
  }

  await User.updateOne({
    password: await bcrypt.hash(req.body.newPassword, 10),
  });

  res.status(200).json({message: 'Password changed successfully'});
}

module.exports = {getUser, deleteUser, changePassword};
