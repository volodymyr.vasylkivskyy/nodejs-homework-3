const express = require('express');
const router = new express.Router();
const {auth} = require('../middlewares/authMiddleWare');
const {asyncWrapper} = require('../helpers');
const {
  getUser,
  changePassword,
  deleteUser,
} = require('../user/userController');

router.get('/', asyncWrapper(auth), asyncWrapper(getUser));

router.patch('/password', asyncWrapper(auth), asyncWrapper(changePassword));

router.delete('/', asyncWrapper(auth), asyncWrapper(deleteUser));

module.exports = router;
