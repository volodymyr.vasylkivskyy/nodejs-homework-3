const express = require('express');
const router = new express.Router();
const {auth} = require('../middlewares/authMiddleWare');
const {asyncWrapper} = require('../helpers');
const {
  addTruck,
  showTrucks,
  showTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('./trucksController');

router.post('/', asyncWrapper(auth), asyncWrapper(addTruck));

router.get('/', asyncWrapper(auth), asyncWrapper(showTrucks));

router.get('/:id', asyncWrapper(auth), asyncWrapper(showTruckById));

router.put('/:id', asyncWrapper(auth), asyncWrapper(updateTruckById));

router.delete('/:id', asyncWrapper(auth), asyncWrapper(deleteTruckById));

router.post('/:id/assign', asyncWrapper(auth), asyncWrapper(assignTruckById));

module.exports = router;
