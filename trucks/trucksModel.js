const mongoose = require('mongoose');
const {Schema} = mongoose;

const truckSchema = new Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  assigned_to: {
    type: String,
  },
  type: {
    type: String,
  },
  status: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
