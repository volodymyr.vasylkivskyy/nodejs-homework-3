const {Truck} = require('../trucks/trucksModel');

/**
 * Add truck
 * @param {obj} req
 * @param {obj} res
 */
async function addTruck(req, res) {
  if (req.user.role === 'DRIVER') {
    const {type} = req.body;
    const truck = new Truck({
      created_by: req.user._id,
      status: 'IS',
      type,
      assigned_to: null,
    });

    await truck.save();
    return res.json({message: 'Truck created successfully'});
  } else {
    return res.json({message: 'Sorry,you are not a driver'});
  }
}

/**
 * Show truck
 * @param {obj} req
 * @param {obj} res
 */
async function showTrucks(req, res) {
  if (req.user.role === 'DRIVER') {
    const userTrucks = await Truck.find(
        {created_by: req.user._id},
        {__v: 0},
    );
    return res.json({trucks: userTrucks});
  } else {
    return res.json({message: 'Sorry,you are not a driver'});
  }
}

/**
 * Show truck by id
 * @param {obj} req
 * @param {obj} res
 */
async function showTruckById(req, res) {
  if (req.user.role === 'DRIVER') {
    const userTruck = await Truck.findOne({
      created_by: req.user._id,
      _id: req.params.id,
    });
    return res.status(200).json({truck: userTruck});
  } else {
    return res.json({message: 'Sorry,you are not a driver'});
  }
}

/**
 * Update truck by id
 * @param {obj} req
 * @param {obj} res
 */
async function updateTruckById(req, res) {
  if (req.user.role === 'DRIVER') {
    const truck = await Truck.findById(req.params.id);
    if (!truck.assigned_to) {
      truck.type = req.body.type;
      await truck.save();
      return res.json({message: 'Truck details changed successfully'});
    } else {
      return res.json({message: 'Your truck is assigned'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a driver'});
  }
}

/**
 * Delete truck by id
 * @param {obj} req
 * @param {obj} res
 */
async function deleteTruckById(req, res) {
  if (req.user.role === 'DRIVER') {
    const truck = await Truck.findById(req.params.id);
    if (!truck.assigned_to) {
      await Truck.findOneAndDelete({
        created_by: req.user._id,
        _id: req.params.id,
      });
      return res.json({message: 'Truck deleted successfully'});
    } else {
      return res.json({message: 'Your truck is assigned'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a driver'});
  }
}

/**
 * Assign truck by id
 * @param {obj} req
 * @param {obj} res
 */
async function assignTruckById(req, res) {
  if (req.user.role === 'DRIVER') {
    const truck = await Truck.find({created_by: req.user._id});
    const assignTruck = truck.filter(
        (item) => Boolean(item.assigned_to) === true,
    );
    if (assignTruck.length == 0) {
      const truck = await Truck.findById(req.params.id);
      truck.assigned_to = truck.created_by;
      await truck.save();
      return res.json({message: 'Truck assigned successfully'});
    } else {
      return res.json({message: 'You have assigned truck'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a driver'});
  }
}

module.exports = {
  addTruck,
  showTrucks,
  showTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
