const express = require('express');
const router = new express.Router();
const {auth} = require('../middlewares/authMiddleWare');
const {asyncWrapper} = require('../helpers');
const {
  addLoad,
  postLoadById,
  updateLoadById,
  deleteLoadById,
  getActiveLoadForDriver,
  changeActiveLoadForDriver,
  getLoadsById,
  getShippingInfoById,
  getUsersLoads,
} = require('./loadController');

router.post('/', asyncWrapper(auth), asyncWrapper(addLoad));
router.post('/:id/post', asyncWrapper(auth), asyncWrapper(postLoadById));
router.put('/:id', asyncWrapper(auth), asyncWrapper(updateLoadById));
router.delete('/:id', asyncWrapper(auth), asyncWrapper(deleteLoadById));
router.get('/active', asyncWrapper(auth), asyncWrapper(getActiveLoadForDriver));
router.patch(
    '/active/state',
    asyncWrapper(auth),
    asyncWrapper(changeActiveLoadForDriver),
);
router.get('/:id', asyncWrapper(auth), asyncWrapper(getLoadsById));

router.get(
    '/:id/shipping_info',
    asyncWrapper(auth),
    asyncWrapper(getShippingInfoById),
);

router.get('/', asyncWrapper(auth), asyncWrapper(getUsersLoads));

module.exports = router;
