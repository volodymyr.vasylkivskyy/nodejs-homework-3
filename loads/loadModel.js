const mongoose = require('mongoose');
const {Schema} = mongoose;

const loadSchema = new Schema({
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    type: {width: Number},
    type: {length: Number},
    type: {height: Number},
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  state: {
    type: String,
  },
  status: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
});

module.exports.Load = mongoose.model('Load', loadSchema);
