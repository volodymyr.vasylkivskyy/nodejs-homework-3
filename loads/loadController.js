const {Load} = require('../loads/loadModel');
const {Truck} = require('../trucks/trucksModel');
const truckTypes = require('../trucks/trucksTypes');

/**
 * Add load
 * @param {obj} req
 * @param {obj} res
 */
async function addLoad(req, res) {
  if (req.user.role === 'SHIPPER') {
    const {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    } = req.body;
    const load = new Load({
      created_by: req.user._id,
      status: 'NEW',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });
    await load.save();

    return res.json({message: 'Load created successfully'});
  } else {
    return res.json({message: 'Sorry,you are not a shipper'});
  }
}

/**
 * Post load
 * @param {obj} req
 * @param {obj} res
 */
async function postLoadById(req, res) {
  if (req.user.role === 'SHIPPER') {
    const trucks = await Truck.find({});
    const postLoad = await Load.findById(req.params.id);
    if (postLoad.status === 'NEW') {
      await Load.findByIdAndUpdate(req.params.id, {
        $set: {status: 'POSTED'},
      });
      const assignTrucks = trucks.filter(
          (item) => Boolean(item.assigned_to) === true,
      );
      const statusTrucks = assignTrucks.filter((item) => item.status === 'IS');
      const featTrucks = statusTrucks.filter(
          (item) => truckTypes[item.type].payload > postLoad.payload,
      );
      let featTruck = [];
      for (let i = 0; i < featTrucks.length; i++) {
        if (
          truckTypes[featTrucks[i].type].height > postLoad.dimensions.height &&
          truckTypes[featTrucks[i].type].width > postLoad.dimensions.width &&
          truckTypes[featTrucks[i].type].length > postLoad.dimensions.length
        ) {
          featTruck = featTrucks[i];
          break;
        }
      }
      if (featTruck.length != 0) {
        await Truck.findByIdAndUpdate(featTruck._id, {
          $set: {status: 'OL'},
        });
        const idUser = featTruck.assigned_to;
        await Load.findByIdAndUpdate(req.params.id, {
          $set: {
            status: 'ASSIGNED',
            state: 'En route to Pick Up',
            assigned_to: featTruck.assigned_to,
            logs: [
              {
                message: `Load assigned to driver with id ${idUser} `,
              },
            ],
          },
        });
        return res.json({
          message: 'Load posted successfully',
          driver_found: true,
        });
      } else {
        await Load.findByIdAndUpdate(req.params.id, {
          $set: {
            status: 'NEW',
            logs: [
              {
                message: `The driver is not found `,
              },
            ],
          },
        });
      }
      return res.json({
        message: 'Load is not posted successfully,because driver is not found',
      });
    } else {
      return res.json({message: 'Sorry,your status is ASSIGNED'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a Shipper'});
  }
}

/**
 * Update load By Id
 * @param {obj} req
 * @param {obj} res
 */
async function updateLoadById(req, res) {
  if (req.user.role === 'SHIPPER') {
    const updateLoad = await Load.findById(req.params.id);
    if (updateLoad.status === 'NEW') {
      const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
      } = req.body;
      await Load.findByIdAndUpdate(req.params.id, {
        $set: {name, payload, pickup_address, delivery_address, dimensions},
      });
      return res.json({message: 'Load details changed successfully'});
    } else {
      return res.json({message: 'Sorry,your status is not NEW'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a shipper'});
  }
}

/**
 * Delete load By Id
 * @param {obj} req
 * @param {obj} res
 */
async function deleteLoadById(req, res) {
  if (req.user.role === 'SHIPPER') {
    const updateLoad = await Load.findById(req.params.id);
    if (updateLoad.status === 'NEW') {
      await Load.findByIdAndDelete(req.params.id);
      return res.json({message: 'Load deleted successfully'});
    } else {
      return res.json({message: 'Sorry,your status is not NEW'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a shipper'});
  }
}

/**
 * Get active load
 * @param {obj} req
 * @param {obj} res
 */
async function getActiveLoadForDriver(req, res) {
  if (req.user.role === 'DRIVER') {
    const activeLoad = await Load.find(
        {assigned_to: req.user._id, status: 'ASSIGNED'},
        {__v: 0},
    );
    // return res.json({activeLoad});
    if (activeLoad.length != 0) {
      // console.log(activeLoad);
      return res.json({load: activeLoad[0]});
    } else {
      return res.json({load: 'You havent got active load'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a DRIVER'});
  }
}

/**
 * Change active load
 * @param {obj} req
 * @param {obj} res
 */
async function changeActiveLoadForDriver(req, res) {
  if (req.user.role === 'DRIVER') {
    const activeLoad = await Load.findOne({
      assigned_to: req.user._id,
      status: 'ASSIGNED',
    });

    if (activeLoad) {
      if (activeLoad.state == 'En route to Pick Up') {
        await Load.findByIdAndUpdate(activeLoad._id, {
          $set: {state: 'Arrived to Pick Up'},
          logs: [
            {
              message: 'Arrived to Pick Up',
            },
          ],
        });
        return res.json({
          message: 'Load state changed to \'Arrived to Pick Up\'',
        });
      }
      if (activeLoad.state == 'Arrived to Pick Up') {
        await Load.findByIdAndUpdate(activeLoad._id, {
          $set: {state: 'En route to delivery'},
          logs: [
            {
              message: 'En route to delivery',
            },
          ],
        });
        return res.json({
          message: 'Load state changed to \'En route to delivery\'',
        });
      }
      if (activeLoad.state == 'En route to delivery') {
        await Load.findByIdAndUpdate(activeLoad._id, {
          $set: {
            state: 'Arrived to delivery',
            status: 'SHIPPED',
            logs: [
              {
                message: 'Your load is shipped',
              },
            ],
          },
        });
        await Truck.findOneAndUpdate(
            {assigned_to: req.user._id},
            {$set: {status: 'IS'}},
        );
        return res.json({
          message: 'Load state changed to \'Arrived to delivery\'',
        });
      }
    } else {
      return res.json({message: 'Sorry,you havent got an active load'});
    }
  } else {
    return res.json({message: 'Sorry,you are not a DRIVER'});
  }
}

/**
 * Get loads by id
 * @param {obj} req
 * @param {obj} res
 */
async function getLoadsById(req, res) {
  const load = await Load.find({
    created_by: req.user._id,
    _id: req.params.id,
  });
  res.json({load});
}

/**
 * Get shipping info by id
 * @param {obj} req
 * @param {obj} res
 */
async function getShippingInfoById(req, res) {
  const load = await Load.find({
    created_by: req.user._id,
    _id: req.params.id,
  });
  res.json({load});
}

/**
 * Get users loads
 * @param {obj} req
 * @param {obj} res
 */
async function getUsersLoads(req, res) {
  if (req.user.role === 'DRIVER') {
    const status = req.query.status;
    if (status) {
      const loads = await Load.find(
          {assigned_to: req.user._id, status},
          [],

          {
            skip: parseInt(req.query.offset),
            limit: parseInt(req.query.limit),
          },
      );
      return res.json({loads});
    }
    const loads = await Load.find(
        {assigned_to: req.user._id},
        [],

        {
          skip: parseInt(req.query.offset),
          limit: parseInt(req.query.limit),
        },
    );
    return res.json({loads});
  }
  if (req.user.role === 'SHIPPER') {
    const status = req.query.status;
    if (status) {
      const loads = await Load.find(
          {created_by: req.user._id, status},
          [],

          {
            skip: parseInt(req.query.offset),
            limit: parseInt(req.query.limit),
          },
      );
      return res.json({loads});
    }
  }
  const loads = await Load.find({created_by: req.user._id}, [], {
    skip: parseInt(req.query.offset),
    limit: parseInt(req.query.limit),
  });
  return res.json({loads});
}

module.exports = {
  addLoad,
  postLoadById,
  updateLoadById,
  deleteLoadById,
  getActiveLoadForDriver,
  changeActiveLoadForDriver,
  getLoadsById,
  getShippingInfoById,
  getUsersLoads,
};
