module.exports = {
  PORT: process.env.PORT || 8080,
  JWT_SECRET: process.env.JWT_SECRET || 'secret',
  DB_USER: process.env.DB_USER || 'vova_test',
  DB_PASS: process.env.DB_PASS || '1234567890',
  DB_NAME: process.env.DB_NAME || 'hw3',
  DB_HOSTNAME: process.env.DB_HOSTNAME || 'cluster0.so72h.mongodb.net',
};
