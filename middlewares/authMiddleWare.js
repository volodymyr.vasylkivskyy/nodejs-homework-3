const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');

module.exports.auth = async (req, res, next) => {
  const reqToken = req.header('Authorization');
  if (!reqToken) {
    return res.status(400).json({message: 'Bad request'});
  }
  const [tokenType, token] = reqToken.split(' ');
  if (tokenType !== 'JWT') {
    return res.status(400).json({message: 'Bad request'});
  }
  const decoded = jwt.verify(token, JWT_SECRET);
  req.user = decoded;
  next();
};
