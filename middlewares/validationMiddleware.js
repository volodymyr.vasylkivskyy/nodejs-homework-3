const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    email: Joi.string().email(),
    role: Joi.string().min(3).max(30),
  });
  await schema.validateAsync(req.body);
  next();
};
