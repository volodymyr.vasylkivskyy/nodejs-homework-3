require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const app = express();

const authRouter = require('./auth/authRouter');
const userRouter = require('./user/userRouter');
const trucksRouter = require('./trucks/trucksRouter');
const loadRouter = require('./loads/loadRouter');

const {PORT} = require('./config');
const {startDB} = require('./server');

app.use(express.json());
app.use(morgan('combined'));

app.all('*', function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
      'Access-Control-Allow-Methods',
      'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  );
  res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  if ('OPTIONS' == req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
});
app.use(express.urlencoded({extended: false}));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

startDB();
app.listen(PORT, () => {
  console.log(`server is working at port ${PORT}`);
});
